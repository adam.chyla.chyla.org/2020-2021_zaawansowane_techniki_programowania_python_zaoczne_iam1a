import re


# search
text = "Alicja ma kota i psa"

# pattern="k.*?a"
# pattern="k[^ ]+a"

match = re.search(pattern="k[^\s]+a",
                  string=text)

if match:
    print("search: znaleziono:", match.group(0))
else:
    print("search: nie znaleziono")


    
# match
# czy podany tekst jest poprawnym kodem pocztowym, XX-XXX
text = "13-321"

# pattern="\d\d-\d\d\d"
# pattern="\d{2}-\d{3}"

match = re.match(pattern="(\d{2})-(\d{3})$",
                 string=text)

if match:
    print("match: poprawny kod", match.group(0))
    print("match: poprawny kod", match.group(1))
    print("match: poprawny kod", match.group(2))
else:
    print("match: bledny kod")

    
    
# findall
# wybierz liczby XX, YYY z kodu pocztowego XX-YYY

text = "12-431"

lista = re.findall(pattern="\d+",
                   string=text)

print("findall:", lista)



