from threading import Thread, Lock
import time

def count(thread_id, results, results_lock):
	results_lock.acquire()

	for i in range(10*thread_id, 10*thread_id+9):
		print(f"thread: {thread_id}, i: {i}")
		results.append(i)
		time.sleep(thread_id)
	
	results_lock.release()


results = []
results_lock = Lock()

threads = [
	Thread(target=count, args=(0, results, results_lock)),
	Thread(target=count, args=(1, results, results_lock)),
	Thread(target=count, args=(2, results, results_lock)),
	Thread(target=count, args=(3, results, results_lock)),
]

for t in threads:
	t.start()
	
for t in threads:
	t.join()

print(results)
	