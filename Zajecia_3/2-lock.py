import threading
import time

class Counter:
	def __init__(self):
		self.value = 0
		
	def increment(self):
		self.value +=  1

		
def count(counter, counter_lock):
	for i in range(10):
		counter_lock.acquire()
		counter.increment()
		counter_lock.release()


counter = Counter()
counter_lock = threading.Lock()

t1 = threading.Thread(target=count, args=(counter, counter_lock))
t2 = threading.Thread(target=count, args=(counter, counter_lock))

t1.start()
t2.start()


t1.join()
t2.join()

print(counter.value)
					  