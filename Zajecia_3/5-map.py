from concurrent.futures import ThreadPoolExecutor

liczby = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#        [op(1), op(2), op(3), ...]  <----

def op(x):
	return x + 10

# map - jednowatkowe
# gen = map(op, liczby)

# map - wielowatkowe
with ThreadPoolExecutor(max_workers=2) as executor:
	gen = executor.map(op, liczby)

wyniki = list(gen)
print(wyniki)
