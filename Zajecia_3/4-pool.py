import threading
from concurrent.futures import ThreadPoolExecutor
import time

class Counter:
	def __init__(self):
		self.value = 0
		
	def increment(self):
		self.value +=  1

		
def count(counter, counter_lock):
	for i in range(10):
		counter_lock.acquire()
		counter.increment()
		counter_lock.release()


counter = Counter()
counter_lock = threading.Lock()

with ThreadPoolExecutor(max_workers=3) as executor:
	executor.submit(count, counter, counter_lock)
	executor.submit(count, counter, counter_lock)
	executor.submit(count, counter, counter_lock)
	executor.submit(count, counter, counter_lock)

print(counter.value)
					  