def dekorator(f):
    def inna_funkcja():
        print("inna funkcja")
        f()

    return inna_funkcja

# funkcja = dekorator(funkcja)
@dekorator
def funkcja():
	print("jestem funkcja")

funkcja()
