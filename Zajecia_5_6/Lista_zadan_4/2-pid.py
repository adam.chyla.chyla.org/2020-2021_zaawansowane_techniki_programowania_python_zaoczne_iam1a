from os import getpid
from multiprocessing import Process


def subprocess():
    print(getpid())


def main():
    processes = [
        Process(target=subprocess),
        Process(target=subprocess),
    ]
    
    for p in processes:
        p.start()
        
    for p in processes:
        p.join()
    
    print(getpid())


if __name__ == "__main__":
    main()
