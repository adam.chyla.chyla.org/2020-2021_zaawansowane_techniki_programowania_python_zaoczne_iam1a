from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor


def potega(x):
    return x ** 2


def main():
    liczby = []
    for _ in range(10):
        x = int(input("Podaj liczbe: "))
        liczby.append(x)

#    with ThreadPoolExecutor(max_workers=2) as pool:
#        output_gen = pool.map(lambda x: x*x, liczby)

    with ProcessPoolExecutor(max_workers=2) as pool:
        output_gen = pool.map(potega, liczby)


    print(list(output_gen))


if __name__ == "__main__":
    main()

