liczby = [1, 2, 3]

it = iter(liczby)

for x in it:
	print(x)

# przebieg 1:
# x = next(it)  # --> 1
# przybieg 2:
# x = next(it)  # --> 2
# przybieg 3:
# x = next(it)  # --> 3
# przebieg 4:
# x = next(it)  # --> StopIteration

	