from itertools import islice

class repeat:
	def __init__(self, elem, times=None):
		self.elem = elem
		self.times = times
		self.current = 0
	
	def __iter__(self):
		return self
	
	def __next__(self):
		if self.times is None or self.current < self.times:
			self.current += 1
			return self.elem
		else:
			raise StopIteration


for x in repeat(10, 3):
	print(x)
	
for x in islice(repeat(10), 0, 10):
	print(x)
