# 5, 6, 7, 8, 9, 10  --  count, islice

from itertools import islice

class count:
	def __init__(self, start):
		self.value = start
	
	def __iter__(self):
		return self
	
	def __next__(self):
		t = self.value
		self.value += 1
		return t


start = int(input("Podaj liczbe: "))
	
for x in islice(count(start), 0, 10):
	print(x)
