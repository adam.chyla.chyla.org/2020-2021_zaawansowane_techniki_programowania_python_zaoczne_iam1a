
class IteratorListy:
	def __init__(self, liczby):
		self.liczby = liczby
		self.index = 0
	
	def __iter__(self):
		return self
	
	def __next__(self):
		if self.index < len(self.liczby):
			value = self.liczby[self.index]
			self.index += 1
			return value
		else:
			raise StopIteration


liczby = [1, 2, 3]

it = IteratorListy(liczby)

for x in it:
	print(x)


# przebieg 1:
# x = next(it)  # --> 1
# przybieg 2:
# x = next(it)  # --> 2
# przybieg 3:
# x = next(it)  # --> 3
# przebieg 4:
# x = next(it)  # --> StopIteration

