# 5, 6, 7, 8, 9, 10  --  count, islice

from itertools import count, islice

for x in islice(count(5), 0, 6):
	print(x)
