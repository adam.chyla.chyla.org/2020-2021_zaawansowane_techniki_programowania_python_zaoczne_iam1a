from concurrent.futures import ProcessPoolExecutor

liczby = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#        [op(1), op(2), op(3), ...]  <----

def op(x):
	return x + 10

def main():
	# map - jednowatkowe
	# gen = map(op, liczby)

	# map - wielowatkowe
	with ProcessPoolExecutor(max_workers=5) as executor:
		gen = executor.map(op, liczby)

	wyniki = list(gen)
	print(wyniki)

if __name__ == '__main__':
	main()
