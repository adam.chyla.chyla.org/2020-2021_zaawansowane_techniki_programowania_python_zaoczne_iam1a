# sum(12!, 19!, 20!)

import multiprocessing

def factorial(n):
	result = 1
	for i in range(1, n+1):
		result = result * i
	return result

def main():
	input_data = [12, 19, 20]
	
	with multiprocessing.Pool(processes=3) as pool:
		output_gen = pool.map(factorial, input_data)

	print("Suma:", sum(output_gen))

main()
