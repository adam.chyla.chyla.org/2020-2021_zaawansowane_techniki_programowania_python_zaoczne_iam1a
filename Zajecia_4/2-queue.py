# sum(12!, 19!, 20!)

import multiprocessing

def factorial(n):
	result = 1
	for i in range(1, n+1):
		result = result * i
	return result

def subprocess(queue, n):
	result = factorial(n)
	queue.put(result)

def main():
	queue = multiprocessing.Queue()
	processes = [
		multiprocessing.Process(target=subprocess, args=(queue, 12)),
		multiprocessing.Process(target=subprocess, args=(queue, 19)),
		multiprocessing.Process(target=subprocess, args=(queue, 20)),
	]
	
	for p in processes:
		p.start()
		
	for p in processes:
		p.join()
		
	suma = 0
	while not queue.empty():
		item = queue.get()
		suma = suma + item

	print("Suma:", suma)

main()
