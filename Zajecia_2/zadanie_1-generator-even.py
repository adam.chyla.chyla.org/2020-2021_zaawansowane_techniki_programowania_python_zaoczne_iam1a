
# to nie jest funkcja, to jest generator!
def even(start=0, stop=None):
	value = start
	while True:
		# dodatek do zadania
		if stop is not None and stop == value:
			return # analogicznie do rzucenia StopIteration w iteratorze
		
		yield value
		value += 2

#for x in even():
#	print(x)

for x in even(4, 10):
	print(x)
	