# count -> 0, 1, 2, 3, 4

# to jest funkcja
def count_ret():
	value = 0
	while True:
		return value
		value += 1

# w zmiennej i znajduje sie zwykla wartosc
i = count_ret()

# to nie jest funkcja, to jest generator (funkcja generujaca)
def count():
	value = 0
	while True:
		yield value   # z funkcji __next__ zwroc value
		value += 1

# w zmiennej it znajduje sie generator
it = count()
		
for x in it:
	print(x)
